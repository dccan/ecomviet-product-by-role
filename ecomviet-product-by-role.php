<?php
/*
Plugin Name: Ecomviet Products By Role
Author: Ecomviet - Dao Cong Can
Description: Show product by user role 
Author URI: 
Version: 1.0
*/
if ( !function_exists( 'add_action' ) ) {
    echo 'Hi there!  I\'m just a plugin, not much I can do when called directly.';
    exit;
}

define( 'ECV_VERSION', '4.1.2' );
define( 'ECV_MINIMUM_WP_VERSION', '4.0' );
define( 'ECV_PLUGIN_DIR', plugin_dir_path( __FILE__ ) );
define( 'ECV_PLUGIN_URL', plugin_dir_url( __FILE__ ) );
define( 'ECV_DELETE_LIMIT', 100000 );


if(is_admin()) {

	require_once( ECV_PLUGIN_DIR. 'includes/admin/class.admin.php' ) ;
	ECOMVIET_ADMIN::init();

} 

register_uninstall_hook(__FILE__,  array( 'ECOMVIET_ADMIN' ,'delete_plugin_database_table' ) );






	
		  
		



