<?php
	class ECOMVIET_ADMIN {

		public static function init(){

			add_action( 'pre_get_posts', array( __CLASS__ ,'ecv_add_role_filter_to_posts_query' ));
			add_action('restrict_manage_posts', array( __CLASS__ ,'ecv_add_role_filter_to_products_administration') ) ;

			add_action('restrict_manage_posts', array( __CLASS__ ,'ecv_add_user_filter_to_products_administration') ) ;
		
		}


		public static function ecv_remove_menu_admin() {

			// remove_menu_page( 'admin_menu','re-upload-image-setting');

		}

		public static function ecv_delete_plugin_database_table() {

		    // delete_option("re_recommend");
		    // add_action('admin_menu', array( __CLASS__, 'remove_menu_admin' ) );

		}

		public static function ecv_add_role_filter_to_posts_query( $query ) {

		    /**
		     * No use on front
		     * pre get posts runs everywhere
		     * even if you test $pagenow after, bail as soon as possible
		     */
		    if ( ! is_admin() ) {
		        return;
		    }
		    global $pagenow;
		    /**
		     * use $query parameter instead of global $post_type
		     */

		    $current_user = wp_get_current_user(); 
		    $role = $current_user->roles;

		    $user_login = $current_user->user_login;

		    if( $role[0] ==  'administrator' ) {

		    	$user_id = '';
		    	if( isset( $_GET['user_id'] ) ) {
		    		$user_id = $_GET['user_id'];
		    		$userById = get_user_by( 'id', $user_id );
		    	}

		    	$user_role = '';
		    	if( isset( $_GET['user_role'] ) ) {
		    		$user_role = $_GET['user_role'];

		    	}

		    	if ( 'edit.php' === $pagenow && 'product' === $query->query['post_type'] ) {

		    		if( !isset( $_GET['user_role'] ) || $_GET['user_role'] == ''   ) {

		    			$query->set( 'author__in', '' );
		    			return;

		    		} else if ( $user_role != '' && $user_id == ''  ) {

			            $role = $_GET['user_role']; // return string 'contributor_facebook'
			            $users   = new WP_User_Query( array( 'role' => $role ) );
			            $results = $users->get_results();

			            $user_ids = array();
			            foreach( $results as $result ) {
			                $user_ids[] = (int) $result->ID;
			            }

			            /**
			             * I use PHP_INT_MAX here cause 0 would not work
			             * this means "user that does not exist"
			             * this trick will make WP_Query return 0 posts
			             */
			            $user_ids = ! empty( $user_ids ) ? $user_ids : PHP_INT_MAX;

			            $query->set( 'author__in', $user_ids );

			        }else if (  $user_id != '' || ( $user_role != '' && $user_role != '' )  ) {

			            $id = $_GET['user_id']; // return string 'contributor_facebook'
			            $users   = new WP_User_Query( array( 'id' => $id ) );

			            $results = $users->get_results();
			            $user_ids = array();

			            if( $user_role == '' ) { 
				            foreach( $results as $result ) {
				            	if ( $id == $result->ID )
					                $user_ids[] = (int) $result->ID;
				            }
				        } else {

				        	foreach( $results as $result ) {
				            	if ( $id == $result->ID && $result->roles[0] == $user_role )
					                $user_ids[] = (int) $result->ID;
				            }

				        }

			            /**
			             * I use PHP_INT_MAX here cause 0 would not work
			             * this means "user that does not exist"
			             * this trick will make WP_Query return 0 posts
			             */
			            $user_ids = ! empty( $user_ids ) ? $user_ids : PHP_INT_MAX;

			            $query->set( 'author__in', $user_ids );

			        }
			        else {
			        	$query->set( 'author__in', 'die' );
			        }

			    } 

		    } else {

		    	if ( 'edit.php' === $pagenow  && 'product' === $query->query['post_type'] ) {

			        if( $role[0] == 'shop_manager' ) {	

			        	$current_user = wp_get_current_user();
			            $users   = new WP_User_Query( array( 'role' => $role ) );
			            $results = $users->get_results();

			            $user_ids = array();
			            foreach( $results as $result ) {
			            	if( $user_login == $result->user_login ) 
			                	$user_ids[] = (int) $result->ID;
			            }
			            /**
			             * I use PHP_INT_MAX here cause 0 would not work
			             * this means "user that does not exist"
			             * this trick will make WP_Query return 0 posts
			             */
			            $user_ids = ! empty( $user_ids ) ? $user_ids : PHP_INT_MAX;
			            $query->set( 'author__in', $user_ids );
			        }   
			    }
		    }
		}

		public static function ecv_add_role_filter_to_products_administration(){

		    //execute only on the 'post' content type

			$current_user = wp_get_current_user(); 
		    $role = $current_user->roles;

		    if( $role[0] == 'administrator' ) {
			    global $post_type;
			    if($post_type == 'product'){

			        $user_role  = '';
			        $user_roles = array();
			        foreach ( get_editable_roles() as $key => $values ) :
			            $user_roles[ $key ] = $values['name'];
			        endforeach;

			        // Set a selected user role
			        if ( ! empty( $_GET['user_role'] ) ) {
			            $user_role  = sanitize_text_field( $_GET['user_role'] );
			        }

			        ?>
			        <select name='user_role'>
			        	<option value=''>
			        		<?php _e( 'All Roles', 'papasemarone' ); ?>
			        	</option>
			        	<?php
			        	foreach ( $user_roles as $key => $value ) :
			            ?>
			            <option <?php selected( $user_role, $key ); ?> value='<?php echo $key; ?>'><?php echo $value; ?>
			            </option>
			            <?php
				        endforeach;
				        ?>
			    	</select>

			        <?php

			    }
		    }

		}


		public static function ecv_add_user_filter_to_products_administration() {
			// Get all user

			$current_user = wp_get_current_user(); 
		    $role = $current_user->roles;
	        $users = get_users();

	        // var_dump($users->data->user_login);
	        // die();

	        if( $role[0] == 'administrator' ) {
			    global $post_type;
			    if($post_type == 'product'){

			        $user_id  = '';
			        $user_ids = array();
			        foreach ( get_users() as $key => $values ) :
			            $user_ids[ $values->data->ID ] = $values->data->user_login;
			        endforeach;

			        // Set a selected user role
			        if ( ! empty( $_GET['user_id'] ) ) {
			            $user_id  = sanitize_text_field( $_GET['user_id'] );
			        }

			        ?>
			        <select name='user_id' style='text-transform: capitalize;' >
			        	<option value=''>
			        		<?php _e( 'All Users', 'papasemarone' ); ?>
			        	</option>
			        	<?php
			        	foreach ( $user_ids as $key => $value ) :
			            ?>
			            <option  <?php selected( $user_id, $key ); ?> value='<?php echo $key; ?>'><?php echo $value; ?>
			            </option>
			            <?php
				        endforeach;
				        ?>
			    	</select>

			        <?php

			    }
		    }

	        // var_dump($users);
	        // die();
		}


	}